resource "random_password" "password" {
  // Create a new password with special chars and 16 characters
  length  = 16
  special = true
}

resource "google_sql_database_instance" "master" {
  // Create a new sql database with variables.tf content
    name             = "main-instance"
    database_version = var.database_version
    region           = var.region


  // We allow internet access only for lab purpose
  settings {
    tier = "db-f1-micro"
    ip_configuration {
      ipv4_enabled = true
      authorized_networks {
        name  = "internet"
        value = "0.0.0.0/0"
      }
    }
  }
}

resource "google_sql_user" "users" {
  // Create the database user
name ="gwenvael"
instance =google_sql_database_instance.master.name
password = random_password.password.result
}

resource "vault_generic_secret" "example" {
  // Put the password in vault
data_json = jsonencode({
  "Metadata" : {
      created_time     = "2022-05-24T11:53:20.903660337Z"
      deletion_time    = "n/a"
      destroyed        = false
      version          = 1
},
  "Data" : {
    password   = 123456
    user       = "glaskar@hipay.com"
}

})
  path = "secret/data/gl_yolo"
}
